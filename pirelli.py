import sys
import aalib
from PIL import Image

import calendar



screen = aalib.AsciiScreen(width=70, height=35)
image = Image.open(sys.argv[1]).convert('L').resize(screen.virtual_size)
screen.put_image((0, 0), image)

chick_lines = screen.render().split('\n')
chick_lines.reverse()
calendar_lines = calendar.calendar(2013).split('\n')

buff_lines = (len(calendar_lines) - len(chick_lines))/2

continue_ = True
#print screen.render()
#print '\n'.join(calendar_lines)
i = 0
for cline in calendar_lines:
    if i >= buff_lines and continue_:
        try:
            print '%-80s %s' % (cline, chick_lines.pop(),)
        except Exception, e:
            print cline
            continue_ = False;
    else:
        print cline
    i += 1

